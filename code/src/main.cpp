
#include <Wire.h>
#include <Adafruit_INA219.h>
#include "Animations.h"

Adafruit_INA219 port0(0x42);
Adafruit_INA219 port1(0x45);
Adafruit_INA219 port2(0x44);
Adafruit_INA219 port3(0x41);
Adafruit_INA219 port4(0x40);
TwoWire bus1;

Animations anim0(&port0);
Animations anim1(&port1);
Animations anim2(&port2);
Animations anim3(&port3);
Animations anim4(&port4);

CRGB* Animations::_leds = nullptr;


unsigned long _previousMillis = 0;
unsigned long  _interval = 5;

void setup(void) 
{

  Serial.begin(115200);
  while (!Serial) {
    delay(1);
  }

  Animations::init();

  bus1.begin(D2,D1);  

  if (! port0.begin(&bus1)) {
  Serial.println("Failed to find INA219 chip on port 0");
  while (1) { delay(10); }
  }

  if (! port1.begin(&bus1)) {
  Serial.println("Failed to find INA219 chip on port 1");
  while (1) { delay(10); }
  }

  if (! port2.begin(&bus1)) {
  Serial.println("Failed to find INA219 chip on port 2");
  while (1) { delay(10); }
  }


  if (! port3.begin(&bus1)) {
  Serial.println("Failed to find INA219 chip on port 3");
  while (1) { delay(10); }
  }

  if (! port4.begin(&bus1)) {
  Serial.println("Failed to find INA219 chip on port 4");
  while (1) { delay(10); }
  }
}

void loop(void) 
{
    unsigned long currentMillis = millis();
    if (currentMillis - _previousMillis >= _interval) 
    {
        _previousMillis = currentMillis;
        anim0.tick();
        anim1.tick();
        anim2.tick();
        anim3.tick();
        anim4.tick();
        FastLED.show();
    }
}


