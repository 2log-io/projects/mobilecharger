#pragma once

#include "Adafruit_INA219.h"
#include "FastLED.h"

#define LED_TYPE            WS2812B
#define COLOR_ORDER         GRB
#define DATA_PIN            D4
#define NUM_LEDS            8


class Animations
{
    public:
        Animations(Adafruit_INA219* sensor);
        void loop();
        uint8 _idx;
        void tick();

        static uint8 getInstanceCount();
        static void init();
        static CRGB* _leds;

    private:
        CRGB interpolateColor(CRGB& from, CRGB& to, uint8 frac, uint8 brightness = 255);
        void setAll(CRGB color);

        float _power;
        uint8 _startLEDIdx;
        uint8 _endLEDIdx;
        static uint8 __instanceCount;
        uint16 _loopCounter = 0;
        Adafruit_INA219* _sensor;
 
        CRGB _pattern[NUM_LEDS];
        bool _easing = true;
        uint8 _pos;
        uint8 _circleSteps;
        unsigned long _previousMillis  = 0;
        unsigned long _interval  = 0;

        CRGB _lastBackground;
        CRGB _newBackground;
        CRGB _lastIndicator;
        CRGB _newIndicator;

        void setPattern(CRGB background, CRGB indicator = CRGB::White);

};