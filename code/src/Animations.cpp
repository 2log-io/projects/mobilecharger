#include "Animations.h"

uint8 Animations::__instanceCount = 0;

DEFINE_GRADIENT_PALETTE( color_gradient ) {
0,    0,      0,    0,   //black
2,    0,      255,    0,   //green
16,     0,      255,    0,   //green
64,     0,      0,      255,   //blue
128,    168,    5,      168,    //purple
255,    247,    25,    26 }; // dark red


#define MAX_POWER 24
#define LED_ORIGIN_OFFSET 0

Animations::Animations(Adafruit_INA219* sensor):
    _idx(__instanceCount++),
    _sensor(sensor)
{
    _circleSteps = 32;
    _interval = 5;
}

uint8 Animations::getInstanceCount(){
    return __instanceCount;
}

void Animations::loop()
{
    unsigned long currentMillis = millis();
    if (currentMillis - _previousMillis >= _interval) 
    {
        _previousMillis = currentMillis;
        tick();
        FastLED.show();
    }
}

void Animations::setPattern(CRGB background, CRGB indicator)
{
    fill_solid(_pattern, NUM_LEDS, background);
    _pattern[0] = indicator;
}

void Animations::setAll(CRGB color)
{
    for(int i = 0; i< NUM_LEDS; i++)
    {
        Animations::_leds[i + (_idx * 8)] = color;
    }
}

void Animations::tick()
{
    _power = ((_power * 5) + (_sensor->getPower_mW() / 1000)) / 6;

    if(_loopCounter == NUM_LEDS * _circleSteps)
    { 
        _lastIndicator = _newIndicator;
       
        _loopCounter = 0;        
        _circleSteps = map(_power, 0, MAX_POWER, 64, 8);
        CRGBPalette16 myPal = color_gradient;
        uint8 selector =  map(_power, 0, MAX_POWER, 0, 255);
        _easing = _power < 10;
        _lastBackground = _newBackground;
        _newBackground = ColorFromPalette(myPal,selector);
        _newIndicator = _power > 2 ? CRGB::White : _newBackground;
    }

   int circle;
   uint8 normalizedLoopCounter = map(_loopCounter,0,NUM_LEDS*_circleSteps,0,255);
   
   if(_easing) 
        circle = map(ease8InOutCubic( normalizedLoopCounter),0,255, 0, NUM_LEDS*_circleSteps);
   else 
        circle = _loopCounter;

   int pos  = circle / _circleSteps;

  
    CRGB indicator =  interpolateColor(_lastIndicator, _newIndicator, normalizedLoopCounter);
    CRGB background = interpolateColor(_lastBackground, _newBackground, normalizedLoopCounter);
    setPattern(background, indicator);

    uint8 brightness =  map(quadwave8(normalizedLoopCounter),0,255,32,128);
    uint8 frac = map(circle % _circleSteps,0, _circleSteps, 0, 255);


    for(int i = 0; i< NUM_LEDS; i++)
    {
        uint8 pos1 = (pos + i ) % 8;
        uint8 pos2 = (pos + i  + 1) % 8;
        uint8 idx;
        uint8 offset = _idx * 8;
        if(false)
            idx = (i + LED_ORIGIN_OFFSET) % 8;   
        else
            idx = ((7 - i) + (8 - LED_ORIGIN_OFFSET)) % 8;

        Animations::_leds[offset + idx] = interpolateColor(_pattern[pos1], _pattern[pos2], frac, brightness);
    }

     _loopCounter++;
}

void Animations::init()
{
    _leds = new CRGB[__instanceCount * NUM_LEDS];
     FastLED.addLeds<LED_TYPE,DATA_PIN,COLOR_ORDER>(_leds, __instanceCount * NUM_LEDS)
       .setCorrection( TypicalLEDStrip );  
}


CRGB Animations::interpolateColor(CRGB& from, CRGB& to, uint8 frac, uint8 brightness)
{
    uint8 red = lerp8by8(from.red, to.red, frac);
    uint8 green = lerp8by8(from.green, to.green, frac);
    uint8 blue = lerp8by8(from.blue, to.blue, frac);

    if(brightness < 255)
    {
        float multiplier = (float) (brightness) / 255;
        red *= multiplier;
        green *= multiplier;
        blue *= multiplier;
    }

    return CRGB(red,green,blue);
}
